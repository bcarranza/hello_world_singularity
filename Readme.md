# Test upload singularity image on gitlab container registry

## Requirement

- singularity == 3.7

## Build image
```bash
$ sudo singularity build hello-world.sif Singularity
$ ./hello-world.sif
Hello, world!
```

## Upload image on gitlab container registry
```bash
$ singularity remote login -u <your_user> oras://registry.gitlab.com/v2
$ singularity push hello-world.sif oras://registry.gitlab.com/philippe_noel/hello_world_singularity/hello-world:1.0
```

## Get image
```bash
$ singularity pull oras://registry.gitlab.com/philippe_noel/hello_world_singularity/hello-world:1.0
$ ./hello-world_1.0.sif
Hello, world!
```
